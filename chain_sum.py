#!/usr/bin/env python3

from math import *
def chain_sum(number):
    result = number
    def wrapper(number2=None):
        nonlocal result
        if number2 is None:
            return result
        result += number2
        return wrapper
    return wrapper

print(chain_sum(5)())
print(chain_sum(5)(7)())
print(chain_sum(5)(99)(-6)())

class chain_sum:
    def __init__(self, number):
        self._number = number

    def __call__(self, value=0):
        return chain_sum(self._number + value)

    def __str__(self):
        return str(self._number)
            
class chain_sum(int):
    def __call__(self, addition=0):
        return chain_sum(self + addition)

    

print(sin(chain_sum(5)))
print(chain_sum(5)(7))
print(chain_sum(5)(99)(-6))
print(chain_sum(5)(7))
print(chain_sum(5)(99)(-6))
