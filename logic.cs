using System;
using System.Linq;

namespace Lesson
{
	class Program
	{
		static void Main(string[] args)
		{
			bool isInfected = true;

			if (!isInfected)
			{
				Console.WriteLine("Персонаж инфицирован!");
			}
			else
			{
				Console.WriteLine("Персонаж здоров!");
			}
		}
	}
}
