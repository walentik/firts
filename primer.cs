using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace Test
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] names = {"Tom","Freed","Win","Queen"};
			IEnumerable<string> filteredNames = names.Where (n => n.Length >= 4);
			foreach (string name in filteredNames)
				Console.Write(name + "|");
		Console.WriteLine("\t\t");
		}
	}
}
